package com.app.blog.config;

import com.app.blog.Security.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;


@Configuration
@EnableMethodSecurity
public class BlogSecurityConfig {

  private CustomUserDetailsService customUserDetailsService;

  @Autowired
  public BlogSecurityConfig(CustomUserDetailsService customUserDetailsService) {
    this.customUserDetailsService = customUserDetailsService;
  }

  @Bean
  public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
    return authenticationConfiguration.getAuthenticationManager();

  }
  @Bean
  public PasswordEncoder passwordEncoder(){
    return new BCryptPasswordEncoder();
  }


  @Bean
  public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
     httpSecurity.csrf().disable().authorizeHttpRequests((auth)->
//                auth.anyRequest().authenticated()
             auth.requestMatchers(HttpMethod.GET, "/api/**").permitAll()
                     .requestMatchers("/swagger-ui/**").permitAll()
                     .requestMatchers("/api-docs").permitAll()
                     .anyRequest().authenticated()
             ).httpBasic(Customizer.withDefaults());

      return httpSecurity.build();
  }

//  @Bean
//  public UserDetailsService userDetailsService(){
//    UserDetails tanmay = User.builder()
//            .username("Tanmay")
//            .password(passwordEncoder().encode("tanm"))
//            .roles("ADMIN")
//            .build();
//
//      UserDetails sam = User.builder()
//              .username("sam")
//              .password(passwordEncoder().encode("sam123"))
//              .roles("USER")
//              .build();
//
//
//    return new InMemoryUserDetailsManager(tanmay, sam);
//  }

}
