package com.app.blog.controller;

import com.app.blog.payload.PostDto;
import com.app.blog.payload.PostResponse;
import com.app.blog.service.PostService;
import com.app.blog.utils.AppConstant;
import jakarta.validation.Valid;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/post")
@NoArgsConstructor
public class PostController {

    private PostService postService;

    @Autowired
    public PostController(PostService postService) {
        this.postService = postService;
    }


    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping()
    public ResponseEntity<String> createPost(@Valid @RequestBody PostDto postDto){
        postService.createPost(postDto);
        return new ResponseEntity<>("Successfully Created" , HttpStatus.CREATED);
    }

    @GetMapping("/get")
    public PostResponse getAll(
            @RequestParam(value = "pageNo", defaultValue = AppConstant.DEFAULT_PAGE_NUMBER,required = false) int pageNo,
            @RequestParam(value = "pageSize",defaultValue = AppConstant.DEFAULT_PAGE_SIZE, required = false) int pageSize,
            @RequestParam(value = "sortBy", defaultValue = AppConstant.DEFAULT_SORT_BY, required = false) String sortBy,
            @RequestParam(value = "sortDir", defaultValue = AppConstant.DEFAULT_SORT_DIR, required = false) String sortDir)
        {

//        PostResponse postResponse = postService.getAll(pageNo , pageSize);

//        return postResponse;
        return postService.getAll(pageNo , pageSize, sortBy,sortDir);
    }

    @GetMapping("/getById/{id}")
    public ResponseEntity<PostDto> getById(@PathVariable("id") long id){
        PostDto postDto = postService.getPost(id);
        return ResponseEntity.ok(postDto);
    }


   @PreAuthorize("hasRole('ADMIN')")
   @PutMapping("/update/{id}")
    public ResponseEntity<String> updatePost(@Valid @RequestBody PostDto postDto, @PathVariable long id){
        postDto.setId(id);
       PostDto postDto1 = postService.updatePost(postDto);

       return ResponseEntity.ok(postDto.getId() +" Successfully Updated.");
    }


    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deletePost(@PathVariable long id){
        postService.deletePost(id);
        return ResponseEntity.ok(id + " deleted Successfully");
    }

}
