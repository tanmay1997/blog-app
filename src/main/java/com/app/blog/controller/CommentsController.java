package com.app.blog.controller;

import com.app.blog.payload.CommentsDto;
import com.app.blog.service.CommentService;
import jakarta.validation.Valid;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/")
@NoArgsConstructor
@Data
public class CommentsController {

    private CommentService commentService;


    @Autowired
    public CommentsController(CommentService commentService) {
        this.commentService = commentService;
    }

//    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/post/{postId}/comment")
    public ResponseEntity<CommentsDto> createComments(@PathVariable(value = "postId") long postId ,@Valid @RequestBody CommentsDto commentsDto){

        CommentsDto commentsDto1 = commentService.createComment(postId, commentsDto);

        return new ResponseEntity<>(commentsDto1 , HttpStatus.CREATED);
    }

    @GetMapping("/post/{postId}/comments")
    public List<CommentsDto> getAllComments(@PathVariable(value = "postId") long postId){

//        List<CommentsDto>  commentsDtos = commentService.getAllComments(postId);

     return commentService.getAllComments(postId);
    }

    @GetMapping("/post/{postId}/comments/{commentId}")
    public ResponseEntity<CommentsDto> getCommentsById(@PathVariable(value = "postId") Long postId,@PathVariable(value = "commentId") Long commentId){

        CommentsDto commentsDto = commentService.getBycommentId(postId, commentId);

        return ResponseEntity.ok(commentsDto);
    }

    @PutMapping("/post/{postId}/comUpdate/{commentId}")
    public ResponseEntity<CommentsDto> updateComments(@PathVariable(value = "postId") Long updateId,@PathVariable(value = "commentId") Long commentId,@Valid @RequestBody CommentsDto commentsDto){

        CommentsDto comment = commentService.updateComment(updateId,commentId,commentsDto);

        return ResponseEntity.ok(comment);
    }

    @DeleteMapping("/post/{postId}/commentdelete/{commentId}")
    public ResponseEntity<String> deleteComment(@PathVariable(value = "postId") Long postId,@PathVariable(value = "commentId") Long commentId){
        commentService.deleteComment(postId,commentId);
        return ResponseEntity.ok(commentId + " deleted.");
    }
}
