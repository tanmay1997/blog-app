package com.app.blog;

import io.swagger.v3.oas.annotations.ExternalDocumentation;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@OpenAPIDefinition(
		info = @Info(
				title="",
				description = "",
				version = "",
				contact = @Contact(
						name = "",
						email = "",
						url = ""
				),
				license = @License(
						name = "",
						url= ""
				)
		),
		externalDocs =  @ExternalDocumentation(
				description="",
				url=""
		)
)
public class BlogAppApplication {


	public static void main(String[] args) {
		SpringApplication.run(BlogAppApplication.class, args);
	}

}
