package com.app.blog.repository;

import com.app.blog.entity.Roles;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RolesRepository extends JpaRepository<Roles, Long>
{
    Optional<Roles> findByRoles (String role);
}
