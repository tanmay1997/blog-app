package com.app.blog.payload;

import com.app.blog.entity.Comment;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;


@Data
public class PostDto {

    private Long id;

    @NotEmpty
    @Size(min = 5 , message = "Title must be between 5 - 15 chars. ")
    private String title;

    @NotEmpty
    @Size(min = 5, message = "Description must not be empty. Enter char more than 5.")
    private String description;

    @NotEmpty(message = "Enter some thing.")
    private String content;

    private Set<CommentsDto> comments;
}
