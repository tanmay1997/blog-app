package com.app.blog.payload;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentsDto {

    private Long id;
    @NotEmpty(message = "Name Should not be empty.")
    private String name;
    @NotEmpty(message = "message should not be empty")
    @Email
    private String email;
    @NotEmpty
    @Size(min = 10, message = "body should not be less than 10 charaters.")
    private String body;

}
