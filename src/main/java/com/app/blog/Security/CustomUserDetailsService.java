package com.app.blog.Security;

import com.app.blog.entity.Users;
import com.app.blog.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private UserRepository userRepository;

    @Autowired
    public CustomUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {

       Users users = userRepository.findByUserNameOrEmail(usernameOrEmail, usernameOrEmail).orElseThrow(()->
               new UsernameNotFoundException("User not found with userName" + usernameOrEmail)
       );

       Set<GrantedAuthority> authorities =  users.getRolesSet().stream().map((role)->
            new SimpleGrantedAuthority(role.getRoles())).collect(Collectors.toSet());

       User user = new User(users.getEmail(),users.getPassword(), authorities);

       return user;

    }
}

