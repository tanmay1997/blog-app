package com.app.blog.service;


import com.app.blog.payload.CommentsDto;

import java.util.List;

public interface CommentService {

    public  CommentsDto createComment(Long postId, CommentsDto commentsDto) ;

    public List<CommentsDto> getAllComments(Long postId);

    public CommentsDto getBycommentId(Long postId, Long commentId);


    public CommentsDto updateComment(Long postId,Long commentId, CommentsDto commentsDto);

    public void deleteComment(Long postId, Long commentId);

}
