package com.app.blog.service.impl;

import com.app.blog.entity.Comment;
import com.app.blog.entity.Post;
import com.app.blog.exception.BlogApiException;
import com.app.blog.exception.ResourceNotFoundException;
import com.app.blog.payload.CommentsDto;
import com.app.blog.repository.CommentsRepository;
import com.app.blog.repository.PostRepository;
import com.app.blog.service.CommentService;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@NoArgsConstructor
public class CommentServiceImpl implements CommentService {

    private ModelMapper modelMapper;
    private CommentsRepository commentsRepository;
    private PostRepository postRepository;


    @Autowired
    public CommentServiceImpl(ModelMapper modelMapper, CommentsRepository commentsRepository, PostRepository postRepository) {
        this.modelMapper = modelMapper;
        this.commentsRepository = commentsRepository;
        this.postRepository = postRepository;
    }

    @Override
    public CommentsDto createComment(Long postId, CommentsDto commentsDto) {

        Comment comment = modelMapper.map(commentsDto, Comment.class);

//      Retrive Post by id
        Post post = postRepository.findById(postId).orElseThrow(()-> new ResourceNotFoundException("Post", "Id", postId));

//       et post to comment entity
        comment.setPosts(post);

        Comment saveComments = commentsRepository.save(comment);

        return  modelMapper.map(saveComments ,CommentsDto.class);
    }

    @Override
    public List<CommentsDto> getAllComments(Long postId) {

        List<Comment> comments = commentsRepository.findByPostsId(postId);

        return comments.stream().map( data-> modelMapper.map(data,CommentsDto.class)).collect(Collectors.toList());
    }

    @Override
    public CommentsDto getBycommentId(Long postId, Long commentId) {

        Post posts = postRepository.findById(postId).orElseThrow(()-> new ResourceNotFoundException("Post","Post Id",postId));

        Comment comment = commentsRepository.findById(commentId).orElseThrow(()-> new ResourceNotFoundException("Comment", "comment Id", commentId));

        if(!comment.getPosts().getId().equals(posts.getId())){
            throw new BlogApiException(HttpStatus.BAD_REQUEST, "Comments doesnot belongs to this post");
        }
        return modelMapper.map(comment, CommentsDto.class);
    }

    @Override
    public CommentsDto updateComment(Long postId, Long commentId, CommentsDto commentsDto) {

        Post post = postRepository.findById(postId).orElseThrow(()->new ResourceNotFoundException("Post", "Post Id", postId));

        Comment comment = commentsRepository.findById(commentId).orElseThrow(()-> new ResourceNotFoundException("Comment","Comment ID", commentId));

        if(!comment.getPosts().getId().equals(post.getId())){
            throw new BlogApiException(HttpStatus.BAD_REQUEST, "Comment does not belongs to this post");
        }
        comment.setName(commentsDto.getName());
        comment.setEmail(commentsDto.getEmail());
        comment.setBody(commentsDto.getBody());
           Comment commentSave = commentsRepository.save(comment);
          return  modelMapper.map(commentSave, CommentsDto.class);
//        return null;
    }

    @Override
    public void deleteComment(Long postId, Long commentId) {
        Post post = postRepository.findById(postId).orElseThrow(()-> new ResourceNotFoundException("Post", "Post Id", postId));

        Comment comment = commentsRepository.findById(commentId).orElseThrow(()-> new ResourceNotFoundException("COmment", "Comment ID", commentId));

        if(!comment.getPosts().getId().equals(post.getId())){
            throw new BlogApiException(HttpStatus.BAD_REQUEST, "Comment not belongs to this post");
        }
        commentsRepository.deleteById(commentId);
    }
}
