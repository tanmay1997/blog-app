package com.app.blog.service.impl;

import com.app.blog.payload.PostDto;
import com.app.blog.entity.Post;
import com.app.blog.exception.ResourceNotFoundException;
import com.app.blog.payload.PostResponse;
import com.app.blog.repository.PostRepository;
import com.app.blog.service.PostService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import org.springframework.data.domain.*;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PostServiceImpl implements PostService {

    private PostRepository postRepository;

    private ModelMapper modelMapper;

    @Autowired
    public PostServiceImpl(PostRepository postRepository, ModelMapper modelMapper) {
        this.postRepository = postRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public PostDto createPost(PostDto postDto) {

        Post post = modelMapper.map(postDto, Post.class);
        Post saved = postRepository.save(post);

        PostDto sendData = modelMapper.map(saved , PostDto.class);

        return sendData;
    }

    @Override
    public PostResponse getAll(int pageNo, int pageSize, String sortBy, String sortDir) {

       Sort sort= sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending() : Sort.by(sortBy).descending();

       Pageable pageable = PageRequest.of(pageNo,pageSize, sort);

       Page<Post> posts = postRepository.findAll(pageable);

        List<Post> postData = posts.getContent();

//        List<PostDto> allDataDto = new ArrayList<>();
//
//        for(Post post: postData){
//            PostDto postDto = new PostDto();
//            postDto.setId(post.getId());
//            postDto.setTitle(post.getTitle());
//            postDto.setDescription(post.getDescription());
//            postDto.setContent(post.getContent());
//            allDataDto.add(postDto);
//        }
        List<PostDto> allDataDto = postData.stream().map((data)-> modelMapper.map(data, PostDto.class)).collect(Collectors.toList());

        PostResponse postResponse = new PostResponse();

        postResponse.setContent(allDataDto);
        postResponse.setPageNo(posts.getNumber());
        postResponse.setPageSize(posts.getSize());
        postResponse.setTotalPages(posts.getTotalPages());
        postResponse.setTotalElement(posts.getTotalElements());
        postResponse.setLast(posts.isLast());

        return postResponse;
    }

    @Override
    public PostDto getPost(Long id) {

        Post isAval = postRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Post", "Id", id));

        PostDto contents = modelMapper.map(isAval,PostDto.class);

        return contents;
    }

    @Override
    public PostDto updatePost(PostDto postDto) {

        Post post = new Post();
        post.setId(postDto.getId());
        post.setTitle(postDto.getTitle());
        post.setContent(postDto.getContent());
        post.setDescription(postDto.getDescription());

        Optional<Post> isAval = postRepository.findById(post.getId());

        PostDto PostDto = new PostDto();

        if(isAval.isPresent()){
           Post savedPost = postRepository.save(post);
           postDto.setId(savedPost.getId());
           postDto.setTitle(savedPost.getTitle());
           postDto.setContent(savedPost.getContent());
           postDto.setDescription(savedPost.getDescription());
        }else {
            throw new ResourceNotFoundException("Post", "Id", postDto.getId());
        }
        return PostDto;
    }

    @Override
    public void deletePost(Long id) {

        Post isAval= postRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Post","id",id));
        postRepository.deleteById(id);

    }
}
