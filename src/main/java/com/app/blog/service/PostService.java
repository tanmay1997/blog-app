package com.app.blog.service;

import com.app.blog.payload.PostDto;
import com.app.blog.payload.PostResponse;

public interface PostService {

    public PostDto createPost(PostDto postDto);

    public PostResponse getAll(int pageNo, int pageSize, String sortBy, String sortDir);

    public PostDto getPost(Long id);

    public PostDto updatePost(PostDto postDto);

    public  void deletePost(Long id);
}
